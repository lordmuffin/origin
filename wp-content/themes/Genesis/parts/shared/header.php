<div id="container">
	<header>
    	<h1><a href="<?php bloginfo( 'wpurl' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
        <div class="clear"></div>
	</header>
    <nav>
	
		<a href="<?php bloginfo( 'wpurl' ); ?>">Home</a>
		
		<?php query_posts('showposts=4&post_parent=0&post_type=page&orderby=the_title&order=ASC'); ?>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
			<!-- Begin Posting information from Query -->
			<a href="?p=<?php the_id(); ?>"><?php the_title() ;?></a>
			<!-- End Posting information from Query -->

			<?php endwhile; else: ?>
			<p>Sorry, there are no posts to display</p>
		<?php endif; ?>
		<?php wp_reset_query();  // Restore global post data ?>

    </nav>
	<div class="clear"></div>
    <div class="grid">